import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.Scanner;

/*5204 负责客户端*/
public class Client {
    public static void main(String[] args) {
        String s;
        Socket mysocket;
        DataInputStream in = null;
        DataOutputStream out = null;
        try {
            mysocket = new Socket("127.0.0.1",2010);
            in = new DataInputStream(mysocket.getInputStream());
            out = new DataOutputStream(mysocket.getOutputStream());

            /*让用户输入中缀表达式*/
            System.out.println("当前为客户端，请输入中缀表达式");
            Scanner reader = new Scanner(System.in);
            String i = reader.nextLine();

            /*把中缀表达式调用MyBC.java的功能转化为后缀表达式*/
            MyBC turn = new MyBC(i);
            s = turn.turnInto();        //s是后缀结果，需要发送给服务器
            System.out.println("在客户端求得后缀表达式："+s);
            /*把后缀表达式通过网络发送给服务器*/
            out.writeUTF(s);            //把s写过去

            /*客户端接收计算的结果*/
            String get = in.readUTF();
            System.out.println("贾普涵负责的客户端接收到的计算结果是："+get);
        }
        catch(Exception e){
            System.out.println("服务器已断开");
        }
    }
}
