import java.io.IOException;
import java.util.Scanner;
import java.util.Stack;

public class MyBC {
    Stack MyStack;
    String expression; //原来的中缀
    String result = ""; //作为结果的后缀

    MyBC(String in) {
        expression = in;
        MyStack = new Stack();
    }

    public String turnInto() {
        for (int j = 0; j < expression.length(); j++) {
            char ch = expression.charAt(j);

            switch (ch) {
                case '+': gotOper(ch, 1); break;
                case '-': gotOper(ch, 1); break;
                case '*': gotOper(ch, 2); break;
                case '/': gotOper(ch, 2); break;
                /*读到左括号压栈*/
                case '(': MyStack.push(ch);            break;
                /*读到右括号，把与最近的一个左括号之间的东西弹出然后加进字符串里*/
                case ')': dumpOut(ch);                  break;
                /*为保证后缀表达式的完整读到数字不输出，加到后缀表达式结果的字符串里*/
                /*添个空格*/
                default:  result = result + " " +ch;       break;
            }
        }
        /*如果栈里还有东西，加到字符串末尾*/
        while (!MyStack.empty()) {
            result = result + " "+MyStack.pop();
        }
        /*字符串result就是所求的后缀表达式*/
        return result;
    }
    /*比较运算符和栈顶元素的优先级*/
    public void gotOper(char opThis, int priotityLevel) {
        while (!MyStack.empty()) {

            /*得到栈顶，但是弹出了的类型是Object需要强转为char*/
            char opTop = (char) MyStack.pop();
            if (opTop == '(') {
                /*栈顶是左括号转到63行，把下一个运算符（onThis）压栈*/
                MyStack.push(opTop);
                break;
            } else {
                /*得到栈顶符号的优先级，记录在x*/
                int x;
                /*加减优先级置1，乘除优先级置2*/
                if (opTop == '+' || opTop == '-') {
                    x = 1;
                } else {
                    x = 2;
                }
                /*处理栈顶的操作符，低优先级压回，高优先级加进结果的字符串result*/
                if (x < priotityLevel) {
                    MyStack.push(opTop);
                    break;
                } else {
                    /*加个空格，再加上栈顶*/
                    result = result +" "+ opTop;
                }
            }
        }
        /*退不退栈顶，最后这个都进栈*/
        MyStack.push(opThis);
    }

    public void dumpOut(char ch) {
        while (!MyStack.empty()) {
            char chx = (char) MyStack.pop(); //强转为char
            if (chx == '(') {
                break;
            } else {
                /*加空格*/
                result = result +" "+ chx;
            }
        }
    }

    public static void main(String[] args) throws IOException {
        Scanner reader = new Scanner(System.in);
        String i = reader.nextLine();

        String output;
        MyBC theTrans = new MyBC(i);
        output = theTrans.turnInto();

        MyDC ltlAndJph = new MyDC();
        int resultNumber = ltlAndJph.evaluate(output);       //计算结果

        System.out.println("后缀结果： " + output);
        System.out.println("计算结果： " + resultNumber);
    }

}
