package ch13.server;

import java.rmi.*;
import java.rmi.server.UnicastRemoteObject;
public class RemoteConcreteSubject extends UnicastRemoteObject implements RemoteSubject{
    double width,height;
    public RemoteConcreteSubject() throws RemoteException {
    }
    @Override
    public void setWidth(double width) throws RemoteException{
        this.width=width;
    }
    @Override
    public void setHeight(double height) throws RemoteException{
        this.height=height;
    }
    @Override
    public double getArea() throws RemoteException {
        return width*height;
    }
}

