package ch13;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Client2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Socket mySocket = null;
        DataInputStream in = null;
        DataOutputStream out = null;
        Thread readData;
        Read read = null;
        try {
            mySocket = new Socket();
            read = new Read();
            readData = new Thread(read);
            System.out.println("输入服务器的IP");
            String IP = scanner.nextLine();
            System.out.println("输入端口号");
            int port = scanner.nextInt();
            if(mySocket.isConnected()){

            }
            else{
                InetAddress address = InetAddress.getByName(IP);
                InetSocketAddress socketAddress = new InetSocketAddress(address,port);
                mySocket.connect(socketAddress);
                in = new DataInputStream(mySocket.getInputStream());
                out = new DataOutputStream(mySocket.getOutputStream());
                read.setDataInputStream(in);
                readData.start();
            }
        }
        catch (Exception e){
            System.out.println("服务器已断开"+e);
        }
        System.out.println("输入圆的半径（放弃请输入N）：");
        while(scanner.hasNext()){
            double radius = 0;
            try{
                radius = scanner.nextDouble();
            }
            catch (InputMismatchException exp){
                System.exit(0);
            }
            try{
                out.writeDouble(radius);
            }
            catch (Exception e){

            }
        }
    }
}
