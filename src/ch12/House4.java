package ch12;

public class House4 implements Runnable {
    int waterAmount;
    Thread dog,cat;
    House4() {
        dog=new Thread(this);  //现在的house4就是线程的目标对象
        cat=new Thread(this);
    }
    public void setWater(int w) {
        waterAmount = w;
    }
    public void run() {
        while(true) {
            Thread t=Thread.currentThread();
            if(t==dog) {                //直接比较引用，没有setName和getName
                System.out.println("家狗喝水") ;
                waterAmount=waterAmount-2;
            }
            else if(t==cat){
                System.out.println("家猫喝水") ;
                waterAmount=waterAmount-1;
            }
            System.out.println(" 剩 "+waterAmount);
            try{  Thread.sleep(2000);
            }
            catch(InterruptedException e){}
            if(waterAmount<=0) {
                return;
            }
        }
    }
}
