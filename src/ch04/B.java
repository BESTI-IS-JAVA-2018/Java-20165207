package ch04;
class B {
	void f() {
		this.g();
		B.h();
	}
	void g() {
		System.out.println("ok");
	}
	static void h() {
		System.out.println("hello");
	}
}
