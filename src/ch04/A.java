package ch04;
class Tom107 {
	int x=120;
	protected int y=20;
	int z=11;
	void f() {
		x = 200;
		System.out.println(x);
	}
	void g() {
		x = 200;
		System.out.println(x);
	}
}
public class A {
	public static void main(String args[]) {
		Tom107 tom = new Tom107();
		tom.x = 22;
		tom.y = 33;
		tom.z = 55;
		tom.f();
		tom.g();
	}
}
