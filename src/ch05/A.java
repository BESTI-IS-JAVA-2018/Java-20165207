package ch05;
class A {
	protected float f(float x,float y) {
		return x-y;
	}
}
class B extends A {
	public float f(float x,float y) {		//降低权限，非法
		return x+y;
	}
}
class C extends A {
	public float f(float x,float y) {	//提高权限，合法
		return x*y;
	}
}
