package ch05;class A5 {
	float computer(float x,float y) {
		return x+y;
	}
	public int g(int x,int y) {
		return x+y;
	}
}
class B5 extends A5 {
	float computer(float x,float y) {
		return x*y;
	}
}
public class Example5_5 {
	public static void main(String args[]) {
		B5 b=new B5();
		double result=b.computer(8,9);		//b调用重写的方法
		System.out.println(result);
		int m=b.g(12,8);
		System.out.println(m);
	}
}
