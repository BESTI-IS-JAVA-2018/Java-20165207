package ch05;public class Pillar1 {
	Circle bottom;
	double height;
	Pillar1 (Circle bottom,double height) {
		this.bottom = bottom;
		this.height = height;
	}
	public double getVolume() {
		return bottom.getArea()*height;
	}
}
