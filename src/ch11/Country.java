package ch11;

import java.sql.*;

public class Country {
    public static void main(String args[]){
        long sum=0;
        double averhigh;
        double averlow;
        String c1="";
        String c2="";
        Connection con=null;
        Statement sql;
        ResultSet rs;
        try{  Class.forName("com.mysql.jdbc.Driver");
        }
        catch(Exception e){}
        String uri = "jdbc:mysql://localhost:3306/world?useSSL=true";
        String user ="root";
        String password ="";
        try{
            con = DriverManager.getConnection(uri,user,password);
        }
        catch(SQLException e){ }
        try {
            sql = con.createStatement();
            rs = sql.executeQuery("SELECT * FROM country");
            rs.next();
            averhigh=rs.getFloat(8);                //给最大最小置初值
            averlow=rs.getFloat(8);

            while(rs.next()){
                float aver=rs.getFloat(8);
                String country = rs.getString(2);
                if((aver<averlow)&&(aver!=0)){
                    averlow=aver;
                    c2=country;
                }
                if((aver>averhigh)&&(aver!=0)){
                    averhigh=aver;
                    c1=country;
                }
            }
            System.out.printf("最大平均寿命%.2f",averhigh);
            System.out.println("这个国家是"+c1);
            System.out.printf("最小平均寿命%.2f",averlow);
            System.out.println("这个国家是"+c2);
        }
        catch (SQLException e){
            System.out.println(e);
        }
    }
}
