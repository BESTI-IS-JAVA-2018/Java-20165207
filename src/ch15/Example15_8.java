package ch15;
import java.util.*;
class Student8 implements Comparable {
   int english=0;
   String name;
   Student8(int english,String name) {
      this.name=name;
      this.english=english;
   }
   public int compareTo(Object b) {
      Student8 st=(Student8)b;
      return (int)(this.english-st.english);
   }
}
public class Example15_8 {
  public static void main(String args[]) {
     TreeSet<Student8> mytree=new TreeSet<Student8>();
     Student8 st1,st2,st3,st4;
     st1=new Student8(90,"赵一");
     st2=new Student8(66,"钱二");
     st3=new Student8(86,"孙三");
     st4=new Student8(76,"李四");
     mytree.add(st1);
     mytree.add(st2);
     mytree.add(st3);
     mytree.add(st4);
     Iterator<Student8> te=mytree.iterator();
     while(te.hasNext()) {
        Student8 stu=te.next();
        System.out.println(""+stu.name+" "+stu.english);
     }
  }
}
