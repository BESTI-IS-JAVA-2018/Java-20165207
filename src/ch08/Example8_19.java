package ch08;public class Example8_19 {
   public static void main(String args[]){
      int n= 12356789;
      System.out.println("整数"+n+"按千分组带正号:");
      String s=String.format("%,+d",n);
      System.out.println(s);
      double number = 98765.6789;
      System.out.println(number+"格式化为整数七位小数三位:");
      s=String.format("%011.3f",number);
      System.out.println(s);
   }
}
