public class MyList {
    public static void main(String [] args) {

        //选用合适的构造方法，用你学号前后各两名同学的学号创建四个结点
        Node<Integer> s1 = new Node<Integer>();
        Node<Integer> s2 = new Node<Integer>();
        Node<Integer> s3 = new Node<Integer>();
        Node<Integer> s4 = new Node<Integer>();
        s1.data = 5205;
        s2.data = 5206;
        s3.data = 5208;
        s4.data = 5209;
        //把上面四个节点连成一个没有头结点的单链表
        s1.next = s2;
        s2.next = s3;
        s3.next = s4;
        s4.next = null;

        //遍历单链表，打印每个结点的
        Node<Integer> s = s1;
        for(int i=0;i<4;i++){
               System.out.println(s.data);
               s=s.next;

        }
        //把你自己插入到合适的位置（学号升序）
        Node<Integer> me = new Node<Integer>(5207,null);
        me.next = s3;
        s2.next = me;
        //遍历单链表，打印每个结点的
        s=s1;
        System.out.println("插入我之后");
        for(int i=0;i<5;i++){
                System.out.println(s.data);
                s=s.next;

        }
        //从链表中删除自己
        s2.next = s3;
        me.next = null;
        //遍历单链表，打印每个结点的
        s=s1;
        System.out.println("删除我之后");
        for(int i=0;i<4;i++){
                System.out.println(s.data);
                s=s.next;
        }
    }
}
