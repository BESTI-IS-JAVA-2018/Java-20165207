import java.util.Comparator;

public class Key  implements Comparator {
    int k=0;
    Key(int k){
        this.k=k;
    }
    @Override
    public int compare(Object o1, Object o2) {
        Key sta = (Key)o1;
        Key stb = (Key)o2;
        if(sta.k-stb.k==0){return -1;}
        else return (sta.k-stb.k);
    }
}
