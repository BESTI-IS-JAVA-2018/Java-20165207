class Undergraduate extends Student{
    private String classID;

    public Undergraduate(String id, String name, char sex, int age,String classID){
        super(id,name,sex,age);
        this.classID=classID;
    }
    public String getClassID(){
        return classID;
    }
    public void setClassID(String classID){
        this.classID=classID;
    }
}