import java.util.*;
import java.io.*;
public class Judge {
	public static void main(String args[]) {
		int L = -2147483648;						//下边界，int能取的最小值
		int H = 2147483647;						//上边界，int能取的最大值
		int MAX,MIN;
		setRange user = new setRange();
		MAX = user.setMAX();
		MIN = user.setMIN();
		if(MAX<MIN){							//如果输入的范围异常，处理
			do{
				System.out.println("MAX<MIN,请重新输入");
				MAX = user.setMAX();
				MIN = user.setMIN();
			}while(MAX<MIN);
		}
		Random rand = new Random();
		int ranNum = rand.nextInt(MAX-MIN+1)+MIN;			//生成随机数,范围MIN-MAX
		char c;								//声明字符型变量，在循环结束用来记录是否继续
		do{								//循环，以防出现异常或者要继续猜
			try{
				System.out.println("开始猜数");                 //开始猜
				Scanner reader = new Scanner(System.in);
				int guessNum=reader.nextInt();					//被猜的数
				if(guessNum==ranNum){                           		//猜对
					System.out.println("猜对了");
				}
				else if((guessNum>=MIN)&&(guessNum<=MAX)&&(guessNum>ranNum)){   //大了
					System.out.println("大了");
				}
				else if((guessNum>=MIN)&&(guessNum<=MAX)&&(guessNum<ranNum)){   //小了
					System.out.println("小了");
				}
				else {
					System.out.println("输入的数不在您设定的范围里");      //不在输入的范围内
				}

			}
			catch(InputMismatchException e){
				System.out.println("输入数值超出了取值范围，异常");
			}
			System.out.println("--------------");
			System.out.println("还猜不？输入y继续");
			Scanner r = new Scanner(System.in);
			String s = r.next();							//没有nextChar()这个方法，先读一个字符串
			c = s.charAt(0);							//读第一个y，转换成字符型，赋值给c
		}while(c=='y');									//c在代码块外面声明，没有被释放
	}
}
