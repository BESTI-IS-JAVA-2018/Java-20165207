import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
/*5207 负责服务器*/
public class Server {
    public static void main(String[] args) {
        String s;           //s是接收过来的后缀表达式的字符串
        ServerSocket serverForClient = null;
        Socket socketOnServer = null;
        DataOutputStream out = null;
        DataInputStream in = null;
        try{
            serverForClient = new ServerSocket(2010);   //与客户端的2010端口一致
        }
        catch(IOException e1){
            System.out.println(e1);
        }
        try{
            System.out.println("等待客户呼叫");
            socketOnServer = serverForClient.accept();
            out = new DataOutputStream(socketOnServer.getOutputStream());
            in = new DataInputStream(socketOnServer.getInputStream());

            /*接收后缀表达式*/
            String i = in.readUTF();

            /*计算后缀表达式的值*/
            MyDC ltlAndJph = new MyDC();
            int resultNumber = ltlAndJph.evaluate(i);       //计算结果
            System.out.println("在李天林负责的服务器端，计算的结果是："+resultNumber);

            Integer I = new Integer(resultNumber);
            s = I.toString();


        }
        catch(Exception e){
            System.out.println("客户已断开"+e);
        }
    }
}

