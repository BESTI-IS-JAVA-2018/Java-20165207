public class MyUtil {
    public static String percentageToFiveGrade(int grade) {
        if((grade<60)&&(grade>=0))        { return "不及格"; }       //成绩小于60，不及格
        else if((grade>=60)&&(grade<70)) { return "及格"; }         //成绩大于等于60小于70，及格
        else if((grade>=70)&&(grade<80)) { return "中等"; }         //成绩大于等于70小于80，中等
        else if((grade>=80)&&(grade<90)) { return "良好"; }         //成绩大于等于80小于90，良好
        else if((grade>=90)&&(grade<=100)){ return "优秀";}         //成绩大于等于90小于等于100，优秀
        else                             { return "错误"; }
    }
}
