public class Complex {
    double RealPart;
    double ImagePart;

    public double getRealPart() {
        return RealPart;
    }

    public double getImagePart() {
        return ImagePart;
    }

    public void setRealPart(double a) {
        RealPart = a;
    }

    public void setImagePart(double a) {
        ImagePart = a;
    }

    Complex() {
    }

    Complex(double r, double i) {
        RealPart = r;
        ImagePart = i;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        else if (o == null) return false;
        else {
            o = (Complex) o;
            double r = ((Complex) o).getRealPart();
            double i = ((Complex) o).getImagePart();
            if ((r == RealPart) && (i == ImagePart)) {
                return true;
            } else return false;
        }
    }

    @Override
    public String toString() {
        return RealPart + "+" + ImagePart + 'i';
    }

    public Complex ComplexAdd(Complex a) {
        Complex com = new Complex();
        com.ImagePart = this.ImagePart + a.ImagePart;
        com.RealPart = this.RealPart + a.RealPart;
        return com;
    }

    public Complex ComplexSub(Complex a) {
        Complex com = new Complex();
        com.ImagePart = this.ImagePart - a.ImagePart;
        com.RealPart = this.RealPart - a.RealPart;
        return com;
    }

    public Complex ComplexMulti(Complex a) {
        Complex com = new Complex();
        com.RealPart = this.RealPart * a.RealPart - this.ImagePart * a.ImagePart;
        com.ImagePart = this.RealPart * a.ImagePart + this.ImagePart * a.RealPart;
        return com;
    }

    public Complex ComplexDiv(Complex a) {
        Complex com = new Complex();
        Complex b = new Complex(a.RealPart, -a.ImagePart);
        System.out.println(b.toString());
        com.RealPart = (this.ComplexMulti(b).RealPart) / (a.RealPart * a.RealPart + a.ImagePart * a.ImagePart);
        com.ImagePart = (this.ComplexMulti(b).ImagePart) / (a.RealPart * a.RealPart + a.ImagePart * a.ImagePart);
        System.out.println(com.ImagePart);
        return com;
    }
}
