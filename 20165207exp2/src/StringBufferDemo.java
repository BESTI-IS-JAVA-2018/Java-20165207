public class StringBufferDemo {
    StringBuffer buffer = new StringBuffer();
    public StringBufferDemo(StringBuffer buffer){
        this.buffer = buffer;
    }
    public StringBuffer append(String s){                   //追加字符串
        buffer.append(s);
        return buffer;
    }
    public StringBuffer append(char c){                     //追加单个字符
        buffer.append(c);
        return buffer;
    }
    public char charAt(int i){                      //获得指定位置上的字符
        char c = buffer.charAt(i);
        return c;
    }
    public int capcity(){                            //获得容量
        int i = buffer.capacity();
        return i;
    }
    public int length(){                             //获得实际长度
        int i = buffer.length();
        return i;
    }
}
