// Server Classes
abstract class Data {
    abstract public void DisplayValue();
}
class Integer extends  Data {
    int value;
    Integer() {
        value=100;
    }
    public void DisplayValue(){
        System.out.println (value);
    }
}
//添加Short类
class Short extends Data{
    short value;
    Short(){
        value=5207;
    }
    public void DisplayValue(){
        System.out.println(value);
    }
}
// Pattern Classes
abstract class Factory {
    abstract public Data CreateDataObject();
}
//原有IntegerFactory
class IntFactory extends Factory {
    public Data CreateDataObject(){
        return new Integer();
    }
}
//07%6=1，我需要支持short
class ShortFactory extends Factory{
    public Data CreateDataObject(){
        return new Short();
    }
}
//Client classes
class Document {
    Data pd;
    Document(Factory pf){
        pd = pf.CreateDataObject();
    }
    public void DisplayData(){
        pd.DisplayValue();
    }
}
//Test class
public class MyDoc {
    static Document d;
    public static void main(String[] args) {
        d = new Document(new ShortFactory());           //修改MyDoc
        d.DisplayData();
    }
}
