public class MyUtilTest {
    public static void main(String[] args) {
        //测试边界情况
        if(MyUtil.percentageToFiveGrade(0) != "不及格")      System.out.println("test boundary failed 1!");
        else if(MyUtil.percentageToFiveGrade(60) != "及格")  System.out.println("test boundary failed 2!");
        else if(MyUtil.percentageToFiveGrade(70) != "中等")  System.out.println("test boundary failed 3!");
        else if(MyUtil.percentageToFiveGrade(80) != "良好")  System.out.println("test boundary failed 4!");
        else if(MyUtil.percentageToFiveGrade(90) != "优秀")  System.out.println("test boundary failed 5!");
        else if(MyUtil.percentageToFiveGrade(100) != "优秀") System.out.println("test boundary failed 6!");
        //测试正常情况
        else if(MyUtil.percentageToFiveGrade(34) != "不及格") System.out.println("test normal failed 1");
        else if(MyUtil.percentageToFiveGrade(62) != "及格")    System.out.println("test normal failed 2");
        else if(MyUtil.percentageToFiveGrade(76) != "中等")    System.out.println("test normal failed 3");
        else if(MyUtil.percentageToFiveGrade(83) != "良好")    System.out.println("test normal failed 4");
        else if(MyUtil.percentageToFiveGrade(99) != "优秀")    System.out.println("test normal failed 5");
        //测试异常情况
        else if(MyUtil.percentageToFiveGrade(-93) != "错误")   System.out.println("test exception failed 1");
        else if(MyUtil.percentageToFiveGrade(123) != "错误")   System.out.println("test exception failed 2");
        else                                                    System.out.println("test passed!");
    }
}
