import junit.framework.TestCase;
import org.junit.Test;
public class ComplexTest extends TestCase {
    Complex x = new Complex(1,2);
    Complex y = new Complex(3,3);
    Complex z = new Complex(1,2);
    Complex a = new Complex(2,3);
    Complex b = new Complex(3,4);
    Complex c = new Complex(3,1);
    Complex d = new Complex(2,3); //a,b,c,d为路人贾添加的测试用例
    @Test
    public void testAdd(){                                          //测试加法
        assertEquals(4.0,x.ComplexAdd(y).RealPart);
        assertEquals(5.0,x.ComplexAdd(y).ImagePart);
        assertEquals(5.0,a.ComplexAdd(c).RealPart);    //路人贾添加的测试用例
        assertEquals(5.0,c.ComplexAdd(b).ImagePart);    //路人贾添加的测试用例
    }
    public void testSub(){                                          //测试减法
        assertEquals(-2.0,x.ComplexSub(y).RealPart);
        assertEquals(-1.0,x.ComplexSub(y).ImagePart);
        assertEquals(1.0,c.ComplexSub(a).RealPart);   //路人贾添加的测试用例
        assertEquals(3.0,b.ComplexSub(c).ImagePart);   //路人贾添加的测试用例
    }
    public void testMulti(){                                        //测试乘法
        assertEquals(-3.0,x.ComplexMulti(y).RealPart);
        assertEquals(9.0,x.ComplexMulti(y).ImagePart);
        assertEquals(3.0,a.ComplexMulti(c).RealPart);   //路人贾添加的测试用例
        assertEquals(17.0,b.ComplexMulti(a).ImagePart);   //路人贾添加的测试用例
    }
    public void testDiv(){                                          //测试除法
        assertEquals(1.0/2.0,x.ComplexDiv(y).RealPart);
        assertEquals(1.0/6.0,x.ComplexDiv(y).ImagePart);
    }
    public void testtoString(){                                     //测试toString
        assertEquals("1.0+2.0i",x.toString());
        assertEquals("2.0+3.0i",a.toString());      //路人贾添加的测试用例
    }       //测试toString
    public void testequals(){
        assertEquals(true,x.equals(z));
        assertEquals(true,a.equals(d));   //路人贾添加的测试用例

    }                 //测试Equals
}