import  org.junit.Test;
import junit.framework.TestCase;
public class MyUtilTest2 extends TestCase {
    @Test
    public void testNormal() {
        assertEquals("不及格", MyUtil.percentageToFiveGrade(55));
        assertEquals("及格", MyUtil.percentageToFiveGrade(65));
        assertEquals("中等", MyUtil.percentageToFiveGrade(75));
        assertEquals("良好", MyUtil.percentageToFiveGrade(85));
        assertEquals("优秀", MyUtil.percentageToFiveGrade(95));
    }
    @Test
    public void testBoundary(){
        assertEquals("不及格",MyUtil.percentageToFiveGrade(0));
        assertEquals("及格",MyUtil.percentageToFiveGrade(60));
        assertEquals("中等",MyUtil.percentageToFiveGrade(70));
        assertEquals("良好",MyUtil.percentageToFiveGrade(80));
        assertEquals("优秀",MyUtil.percentageToFiveGrade(90));
        assertEquals("优秀",MyUtil.percentageToFiveGrade(100));
    }
    @Test
    public void testException(){
        assertEquals("错误",MyUtil.percentageToFiveGrade(-9));
        assertEquals("错误",MyUtil.percentageToFiveGrade(200));
    }
}