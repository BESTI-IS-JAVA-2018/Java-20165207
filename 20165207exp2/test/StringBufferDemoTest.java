import junit.framework.TestCase;
import org.junit.Test;
public class StringBufferDemoTest extends TestCase {
    StringBuffer buffer1 = new StringBuffer("StringBuffer");
    StringBuffer buffer2 = new StringBuffer("StringBuffer20165207exp2");
    @Test
    public void testcharAt(){                                           //测试charAt方法
        assertEquals('2',buffer2.charAt(12));
        assertEquals('r',buffer1.charAt(11));
    }
    @Test
    public void testcapcity(){                                          //测试容量
        assertEquals(28,buffer1.capacity());
        assertEquals(40,buffer2.capacity());
    }
    public void testlength(){                                           //测试实际长度
        assertEquals(12,buffer1.length());
        assertEquals(24,buffer2.length());
    }
}