import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5forRickAndMorty {
    public static void main(String args[]) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        String info = args[0];
        MessageDigest m = MessageDigest.getInstance("MD5");
        m.update(info.getBytes("UTF8"));
        byte s[ ]=m.digest( );
        String result="";
        for (int i=0; i<s.length; i++){
            result+=Integer.toHexString((0x000000ff & s[i])| 0xffffff00).substring(6);
        }
        System.out.println(result);
    }
}
