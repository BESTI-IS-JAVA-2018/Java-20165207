/**
 * @author ltl20165207
 */
public class CodeStandard {
    @Override
    public String toString() {
        return "这是用code中的Generate生成的toString的重写方法"+super.toString();
    }

    public static void main(String[] args) {
        /*避免魔法值*/
        int j = 20;
        StringBuilder buffer = new StringBuilder();
        buffer.append('S');
        buffer.append("tringBuffer");
        System.out.println(buffer.charAt(1));
        System.out.println(buffer.capacity());
        System.out.println(buffer.indexOf("tring"));
        buffer.toString();
        System.out.println("buffer = " + buffer.toString());
        if (buffer.capacity() < j) {
            buffer.append("1234567");
        }
        for (int i = 0; i < buffer.length(); i++) {
            System.out.println(buffer.charAt(i));
        }
    }
}