public class SumofRecur {
	public static void main (String args[]) {
		int sum=0,i=1,t=1;
		int N = Integer.parseInt(args[0]);
		for(;i<=N;i++) {
			t=t*i;
			sum=sum+t;
		}
		System.out.println(sum);
	}
}
