public class week6test1 {
	public static void main(String args[]){
		int m = Integer.parseInt(args[0]);
		int n = Integer.parseInt(args[1]);
		int c = dg(m,n);
		if(c!=-1)
		System.out.println(c);
	}
	public static int dg(int m,int n) {
		if(m<n){
			System.out.printf("m<n\n");
			return -1;
		}
		if(m==n){
			return 1;
		}
		else if(n==0) {
			return 0;
		}
		else if(n==1) {
			return m;
		}
		else{
			return (dg(m-1,n)+dg(m-1,n-1));
		}
	}
}
