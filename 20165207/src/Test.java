public class Test {
	public static void main(String args[]) {
		CPU cpu = new CPU(2200);
		HardDisk disk = new HardDisk(200);
		PC pc = new PC();
		pc.setCPU(cpu);
		pc.setHardDisk(disk);
		pc.show();
	}
}
