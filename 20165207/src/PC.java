class PC {
	CPU cpu;
	HardDisk HD;
	PC(){}
	PC(CPU c){cpu=c;}
	PC(HardDisk h){HD=h;}
	PC(CPU c,HardDisk h){
		cpu=c;
		HD=h;
	}
	void setCPU(CPU c){
		cpu = c;
	}
	void setHardDisk(HardDisk h){
		HD = h;
	}
	void show() {
		System.out.println("cpu的速度是："+cpu.getSpeed());
		System.out.println("硬盘容量为："+HD.getAmount());
	}
}
